module gitee.com/spwx/errors

go 1.15

require (
	github.com/bytedance/sonic v1.8.3 // indirect
	github.com/cloudwego/hertz v0.6.0
	github.com/gin-gonic/gin v1.9.0
	github.com/go-sql-driver/mysql v1.7.0
	github.com/klauspost/cpuid/v2 v2.2.4 // indirect
	github.com/leodido/go-urn v1.2.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pelletier/go-toml/v2 v2.0.7 // indirect
	github.com/ugorji/go/codec v1.2.10 // indirect
	golang.org/x/arch v0.2.0 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	google.golang.org/genproto v0.0.0-20230301171018-9ab4bdc49ad5 // indirect
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
	gorm.io/gorm v1.24.6
)
